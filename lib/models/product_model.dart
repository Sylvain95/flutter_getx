import 'package:flutter/material.dart';

class Product{
  final String name;
  final double price;
  final String imageUrl;

  const Product({required this.name, required this.price, required this.imageUrl});
  /*static Product FromSnapshop(DocumentSnapshop snap){
    Product product = Product(
        name: snap['name'], price: snap['price'], imageUrl: snap['imageUrl']);
    return product;
  }*/
  static const List<Product> products = [
    Product(
        name: "Mangue",
        price: 90.3,
        imageUrl: "assets/images/mangue.jpeg"
    ),
    Product(
        name: "Boutique",
        price: 40.5,
        imageUrl: "assets/images/boutique.jpeg"
    ),
    Product(
        name: "Citron",
        price: 30.3,
        imageUrl: "assets/images/citron.jpeg"
    ),
    Product(
        name: "Papaye",
        price: 200.3,
        imageUrl: "assets/images/papaye.jpeg"
    ),
    Product(
        name: "Avocat",
        price: 100.3,
        imageUrl: "assets/images/avocat.jpeg"
    ),
    Product(
        name: "BT",
        price: 50.6,
        imageUrl: "assets/images/bt.jpeg"
    ),
    Product(
        name: "Mangue",
        price: 90.3,
        imageUrl: "assets/images/mangue.jpeg"
    ),
    Product(
        name: "Boutique",
        price: 40.5,
        imageUrl: "assets/images/boutique.jpeg"
    ),
    Product(
        name: "Citron",
        price: 30.3,
        imageUrl: "assets/images/citron.jpeg"
    ),
  ];
  static const List<Product> products1 = [
    Product(
        name: "Mangue",
        price: 1.3,
        imageUrl: "https://static4.depositphotos.com/1020804/359/i/600/depositphotos_3598470-stock-photo-mango-with-section-on-a.jpg"
    ),
    Product(
        name: "Orange",
        price: 2.5,
        imageUrl: "https://img-3.journaldesfemmes.fr/a5LFTZ3qU2fUVOmwIVKDJawBJXA=/1500x/smart/83c0e4f55dd846dea2be0be27e715dcd/ccmcms-jdf/10662446.jpg"
    ),
    Product(
        name: "Citron",
        price: 3.3,
        imageUrl: "https://st.depositphotos.com/1020804/2370/i/600/depositphotos_23707225-stock-photo-lemons-with-leaves.jpg"
    ),
    Product(
        name: "Papagne",
        price: 2.3,
        imageUrl: "https://www.lesproduitsnaturels.com/userfiles/www.lesproduitsnaturels.com/images/papaye.jpg"
    ),
    Product(
        name: "Avocat",
        price: 2.3,
        imageUrl: "https://tous-les-fruits.com/wp-content/uploads/2018/02/avocat.jpg"
    ),
    Product(
        name: "Mangue",
        price: 2000.0,
        imageUrl: "https://static4.depositphotos.com/1020804/359/i/600/depositphotos_3598470-stock-photo-mango-with-section-on-a.jpg"
    ),
    Product(
        name: "Orange",
        price: 1000.5,
        imageUrl: "https://img-3.journaldesfemmes.fr/a5LFTZ3qU2fUVOmwIVKDJawBJXA=/1500x/smart/83c0e4f55dd846dea2be0be27e715dcd/ccmcms-jdf/10662446.jpg"
    ),
    Product(
        name: "Papagne",
        price: 20.3,
        imageUrl: "https://www.lesproduitsnaturels.com/userfiles/www.lesproduitsnaturels.com/images/papaye.jpg"
    ),
    Product(
        name: "Avocat",
        price: 989.3,
        imageUrl: "https://tous-les-fruits.com/wp-content/uploads/2018/02/avocat.jpg"
    )
  ];
}