import 'package:flutter/material.dart';
import 'package:flutter_getx/controllers/cart_controller.dart';
import 'package:flutter_getx/models/product_model.dart';
import 'package:get/get.dart';

class CartProducts extends StatelessWidget {
  final CartController controller = Get.find();
  CartProducts({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(
          ()=>
              controller.products.length>0 ?ListView.builder(//length
                  itemCount: controller.products.length,
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, int index){
                    return CartProductCard(
                      controller: controller,
                      product: controller.products.keys.toList()[index],
                      quantity: controller.products.values.toList()[index],
                      index: index,
                    );
                  }
              ):const SizedBox(
                  height: 800,
                child: Center(
                  child: Text("Cart empty")
                ),
              ),
    );
  }
}

class CartProductCard extends StatelessWidget {
  final CartController controller;
  final Product product;
  final int quantity;
  final int index;
  const CartProductCard({
    Key? key,required this.index,required this.controller,required this.product,required this.quantity}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          CircleAvatar(
            radius: 40,
            backgroundImage: AssetImage(product.imageUrl),
          ),
          const SizedBox(width: 20),
          Expanded(
              child: Text(product.name,
                style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
              )
          ),
          IconButton(
            onPressed: (){
              controller.removeProduct(product);              },
            icon: const Icon(Icons.remove_circle),
          ),
        Text("$quantity"),
          IconButton(
            onPressed: (){
              controller.addProduct(product);              },
            icon: const Icon(Icons.add_circle),
          )
        ],
      ),
    );
  }
}
