import 'package:flutter_getx/models/product_model.dart';
import 'package:get/get.dart';

class CartController extends GetxController {
  final _products = {}.obs;
  
  void addProduct(Product product){
    if(_products.containsKey(product)){
      _products[product] += 1;
    }else{
      _products[product] = 1;
    }
    /*
    Get.snackbar("Produit ajoute",
        "Vous avez ajoute le ${product.name} dans cart",
      snackPosition: SnackPosition.BOTTOM,
      duration: const Duration(seconds: 2),
    );
     */
  }

  get products => _products;

  void removeProduct(Product product) {
    if(_products.containsKey(product) && _products[product] == 1){
      _products.removeWhere((key, value) => key == product);
    }else{
      _products[product] -= 1;
    }
  }
  get productSubtotal => _products.entries
      .map((product) => product.key.price * product.value)
      .toList();
  get total => _products.entries.isNotEmpty?_products.entries
      .map((product) => product.key.price * product.value)
      .toList()
      .reduce((value, element) => value + element).toStringAsFixed(2):0.0;
}