import 'package:flutter/material.dart';
import 'package:flutter_getx/widgets/cart_product.dart';
import 'package:flutter_getx/widgets/cart_total.dart';

class CartScreen extends StatelessWidget {
  const CartScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("My carts"),elevation: 0,),
      body: CartProducts(),
      bottomSheet: Padding(
        padding: const EdgeInsets.only(bottom: 5),
        child: CartTotal()
      ),
    );
  }
}
