import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_getx/controllers/cart_controller.dart';
import 'package:flutter_getx/models/product_model.dart';
import 'package:flutter_getx/widgets/catalog_product.dart';
import 'package:get/get.dart';

import 'cart_screen.dart';

class CatalogScreen extends StatelessWidget {
  CatalogScreen({Key? key}) : super(key: key);
  //final CartController controller = Get.find();
  final controller = Get.put(CartController());
/*
              ElevatedButton(
                  onPressed: ()=>Get.to(() => const CartScreen()),
                  child: const Text("Go to card")
              )
               */
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Shopping Cart App"),
        centerTitle: true,
        elevation: 0,
        actions: [
          Padding(
              padding: const EdgeInsets.only(right: 25,top: 9),
            child:  Obx(() =>InkWell(
              onTap: ()=>Get.to(() => const CartScreen()),
              child:  Badge(
                  badgeContent: Text(
                    "${controller.products.length}",
                    style: const TextStyle(color: Colors.white),
                  ),
                  padding: const EdgeInsets.all(3.8),
                  //badgeColor: Colors.white,
                  showBadge: controller.products.length > 0,
                  position: BadgePosition.topEnd(top: -10, end: -4),
                  child: const Icon(Icons.shopping_cart_outlined)
              ),
            )
            ),
          )
        ],
      ),
      body: SafeArea(
          child: ListView.builder(
              itemCount: Product.products.length,
              itemBuilder: (BuildContext context, int index){
                return CatalogProductCard(index: index);
              }
          )
      ),
    );
  }
}

